using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    #region Main Methods

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) SceneManager.LoadScene("House");
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    }

    #endregion
}
