using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour
{
    #region Variables

    //Components
    Animator anim;
    NavMeshAgent agent;
    Transform player;

    //Movement
    public float speedSmoothTime;
    [HideInInspector]
    public bool walk;
    [HideInInspector]
    public bool persecution;
    [HideInInspector]
    public bool activate;

    //Animation
    float animationBehaviour;
    [HideInInspector]
    public Material skin;
    [HideInInspector]
    public float dissolve;

    #endregion

    #region Main Methods

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        player = GameObject.Find("Player").transform;        
    }

    private void Update()
    {        
        persecution = activate && Input.GetKey(KeyCode.LeftShift); //Causes the enemy to run if the player runs

        if (persecution) walk = false; //If you run deactivate the walking animation
        if (Input.GetKeyUp(KeyCode.LeftShift) && activate) walk = true; //When you stop running, it reactivates the walking animation.

        if (walk) 
        {            
            animationBehaviour = .5f;
            anim.SetFloat("Behaviour", animationBehaviour, speedSmoothTime, Time.deltaTime);
            agent.speed = 1.7f;
            agent.SetDestination(player.position);
        }

        else if (persecution)
        {
            walk = false;
            animationBehaviour = 1;
            anim.SetFloat("Behaviour", animationBehaviour, speedSmoothTime, Time.deltaTime);

            agent.speed = 3.2f;
            agent.SetDestination(player.position);
        }

        else //Idle
        {
            animationBehaviour = 0;
            anim.SetFloat("Behaviour", animationBehaviour, speedSmoothTime, Time.deltaTime);
            agent.speed = 0; 
        }
    }

    #endregion

}
