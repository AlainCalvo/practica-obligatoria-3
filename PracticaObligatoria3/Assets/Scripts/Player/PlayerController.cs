using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables

    [Header("---References---")]
    public CharacterController cc;
    public Transform cam;
    Animator anim;
    GameManager gm;

    [Header("---Speed---")]
    public float walkSpeed;
    public float runSpeed;
    float currentSpeed;
    public bool canWalk = true;
    [HideInInspector]
    public Vector3 dir;

    [Header("---Speed smooth---")]
    public float speedSmoothTime;
    float speedSmoothVelocity;

    [Header("---Camera---")]
    float targetAngle;
    float angle;
    bool camBlend;
    
    [Header("---Camera smooth---")]
    public float turnSmoothTime;
    float turnSmoothVelocity;

    [Header("---Gravity---")]
    public float gravity = -9.81f;
    public Vector3 velocity;

    //House.
    [HideInInspector]
    public bool bathroom;
    [HideInInspector]
    public bool exit;
    [HideInInspector]
    public bool sofa;
    [HideInInspector]
    public bool coffe;
    [HideInInspector]
    public bool drink;
    [HideInInspector]
    public bool disappear;
    #endregion

    #region Main Methods

    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (drink) //Drinking animation.
        {
            disappear = true;
            anim.SetBool("drinking", true);
            StartCoroutine(MugUp());
        }
       
        if (canWalk)
        {
            float dirHor = Input.GetAxisRaw("Horizontal");
            float dirVEr = Input.GetAxisRaw("Vertical");
            dir = new Vector3(dirHor, 0f, dirVEr).normalized;

            if(camBlend ) anim.SetFloat("speedPercent", 0, 0,0); //Blocks the character's movement animation while we are under the effects of the camera change.

            //Sets the movement animation between walking and running.
            bool running = Input.GetKey(KeyCode.LeftShift); //Sprint button
            float animationSpeedPercent = (running ? 1 : .5f) * dir.magnitude;
            anim.SetFloat("speedPercent", animationSpeedPercent, speedSmoothTime, Time.deltaTime);

            if (dir.magnitude >= 0.1f && !camBlend) //If we are giving a direction and we are not under the effects of the camera transition.
            {
                targetAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                float targetSpeed = (running ? runSpeed : walkSpeed) * dir.magnitude;
                currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);

                Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

                cc.Move(moveDir.normalized * currentSpeed * Time.deltaTime);  //Motion using the currently active camera as reference
            }

            velocity.y += gravity * Time.deltaTime;

            cc.Move(velocity * Time.deltaTime); //Gravity.
        }            
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Used by cinemachine to launch the effect on the player of the transition of fixed cameras.
    /// </summary>
    public void CameraBlend()
    {
        StartCoroutine(ChangeCamera());
    }

    /// <summary>
    /// Controls the duration of the effect on the player in the transition between fixed cameras.
    /// </summary>
    /// <returns></returns>
    IEnumerator ChangeCamera()
    {
        camBlend = true;
        yield return new WaitForSeconds(.5f);
        camBlend = false;
    }

    /// <summary>
    /// Controls the animation of coffee drinking.
    /// </summary>
    /// <returns></returns>
    IEnumerator MugUp()
    {
        canWalk = false;        
        yield return new WaitForSeconds(8.26f);
        anim.SetBool("drinking", false);
        yield return new WaitForSeconds(0.5f);
        disappear = false;
        canWalk = true;
        drink = false;
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy") gm.dead = true; //Launches the death of the player.

        //Control of interactions in the home.
        if (other.gameObject.name == "Bathroom") bathroom = true;
        if (other.gameObject.name == "Exit") exit = true;
        if (other.gameObject.name == "Sofa") sofa = true;
        if (other.gameObject.name == "Kitchen") coffe = true;
    }

    private void OnTriggerExit(Collider other)
    {
        //Home interaction control.
        if (other.gameObject.name == "Bathroom") bathroom = false;
        if (other.gameObject.name == "Exit") exit = false;
        if (other.gameObject.name == "Sofa") sofa = false;
        if (other.gameObject.name == "Kitchen") coffe = false;
    }

    #endregion

}
