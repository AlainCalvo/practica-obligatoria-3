using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Presentation : MonoBehaviour
{
    #region Variables

    [Header("---References---")]
    public GameObject clown;
    GameManager gm;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {     
        //Activate first clown
        if(other.gameObject.tag == "Player")
        {
            clown.SetActive(true);
            gm.introduce = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Activate first clowns text
        if (other.gameObject.tag == "Player") gm.clownIntro = true;
    }

    #endregion
}
