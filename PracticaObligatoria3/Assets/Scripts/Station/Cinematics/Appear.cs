using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Appear : MonoBehaviour
{
    #region Variables

    GameManager gm;
    GameObject clown;
    PlayerController pc;
    
    bool walk;
    bool loop;

    #endregion

    #region Main Methods

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        clown = GameObject.Find("Enemy2");
        pc = FindObjectOfType<PlayerController>();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Puts the second clown to chase the player
    /// </summary>
    /// <returns></returns>
    IEnumerator Persecution()
    {
        yield return new WaitForSeconds(6.47f);
        clown.GetComponent<Movement>().walk = true;
        clown.GetComponent<Movement>().activate = true;
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        if(!loop && other.gameObject.tag == "Player")
        {
            pc.canWalk = false;
            gm.appear = true; //Issue an order to deactivate the first clown and activate the second one.
            loop = true;
        }

        if (loop && other.gameObject.tag == "Player")
        {
            StartCoroutine(Persecution()); //Activates the pursuit to the end
        }
    }

    private void OnTriggerExit(Collider other)
    {     
        this.gameObject.SetActive(false);
    }

    #endregion
}
