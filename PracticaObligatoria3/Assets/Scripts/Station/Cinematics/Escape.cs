using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escape : MonoBehaviour
{
    #region Variables

    public GameObject text;
    public GameObject player;
    public GameObject clown;

    GameManager gm;

    bool interactable;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (interactable && Input.GetKeyDown(KeyCode.E))
        {
            gm.escape = true;
            player.SetActive(true);
            clown.SetActive(true);
        }
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            text.SetActive(true);
            interactable = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            text.SetActive(false);
            interactable = false;
        }
    }

    #endregion
}
