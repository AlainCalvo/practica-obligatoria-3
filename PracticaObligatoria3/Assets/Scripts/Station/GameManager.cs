using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region variables;
           
    [Header("---Enemies---")]
    public GameObject clown1;
    public GameObject clown2;
    GameObject enemy1;
    [HideInInspector]
    public bool clownIntro;        
    [HideInInspector]    
    public bool introduce;

    [Header("---UI---")]
    public GameObject text1;
    public GameObject wagonText;
    public GameObject endScreen;
    public GameObject pauseMenu;
    public GameObject dieScreen;

    [Header("---STATION---")]
    public GameObject wagon1;
    public GameObject wagon2;
    Door door;

    [Header("---TIMELINE---")]
    public GameObject launcher;
    public GameObject appeDirector;
    public GameObject escapeDirector;
    [HideInInspector]
    public bool appear;
    [HideInInspector]
    public bool escape; 

    //Player
    PlayerController pc;
    [HideInInspector]
    public bool dead;

    bool gameIsPaused;

    #endregion;

    #region Main Methods

    private void Start()
    {        
        pc = FindObjectOfType<PlayerController>();
        door = FindObjectOfType<Door>();
        enemy1 = GameObject.Find("Enemy1");
    }

    private void Update()
    {
        //Player
        if (dead) StartCoroutine(Murder()); //If you die, the season starts again.

        //Enemigos
        if (introduce)IntroduceTheFirst();
        if (clownIntro) StartCoroutine(FirstText());
        
        //Cinematicas
        if (appear) 
        {
            ChangeClown();
            wagon2.SetActive(true);
            pc.canWalk = true;
        }
        if (escape)
        {
            escapeDirector.SetActive(true);
            wagonText.SetActive(false);
        }

        //Station
        NoEscape();

        if (endScreen.activeSelf == true) 
        {
            StartCoroutine(BackToMenu());
            dieScreen.SetActive(false);
        }

        //Menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf); //Open pause screen.
            gameIsPaused = !gameIsPaused;

            if (gameIsPaused)
            {
                Time.timeScale = 0;
            }
            else Resume();
        }

        if (pauseMenu.activeSelf == true) Cursor.lockState = CursorLockMode.None;
        else Cursor.lockState = CursorLockMode.Locked;
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Activate pause menu
    /// </summary>
    private void PauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf); //Open pause screen
            gameIsPaused = !gameIsPaused;

            if (gameIsPaused)
            {
                Time.timeScale = 0;
            }
            else Resume();     
        }
    }

    /// <summary>
    /// Sets the first clown in motion
    /// </summary>
    private void IntroduceTheFirst()
    {
        enemy1.GetComponent<Movement>().activate = true;
        enemy1.GetComponent<Movement>().walk = true;
    }

    /// <summary>
    /// Activate the dialog box when the first clown appears.
    /// </summary>
    /// <returns></returns>
    IEnumerator FirstText()
    {
        text1.SetActive(true);
        yield return new WaitForSeconds(2);
        text1.SetActive(false);        
        clownIntro = false;

    }

    /// <summary>
    /// Deactivate the first clown, activate the second clown, and launch the appearance cinematic.
    /// </summary>
    private void ChangeClown()
    {
        clown1.SetActive(false);
        clown2.SetActive(true);
        appeDirector.SetActive(true);
    }

    /// <summary>
    /// If we do not lock up the first clown, there is no meter and the second clown is not activated.
    /// </summary>
    private void NoEscape()
    {
        if (!door.close)
        {
            wagon1.SetActive(false);
            launcher.SetActive(false);
        }

        else
        {
            wagon1.SetActive(true);
            launcher.SetActive(true);
        }
    }

    /// <summary>
    /// Back to menu.
    /// </summary>
    /// <returns></returns>
    IEnumerator BackToMenu()
    {
        yield return new WaitForSeconds(5);
        LeaveGame();
    }

    /// <summary>
    /// Reset the time and close the pause screen.
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        gameIsPaused = false;
    }

    /// <summary>
    /// Back to the main menu from the pause menu
    /// </summary>
    public void LeaveGame()
    {
        gameIsPaused = false;
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Controls the player's death.
    /// </summary>
    /// <returns></returns>
    IEnumerator Murder()
    {
        dieScreen.SetActive(true);
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene("Station");
    }

    #endregion
}
