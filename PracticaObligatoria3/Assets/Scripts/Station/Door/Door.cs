using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    #region Variables

    [Header("---References---")]
    public GameObject closeText;
    public GameObject warningText;
    public GameObject clown;
    GameManager gm;
    Movement move;

    [Header("---Door lock---")]
    public float speed;
    float angle;
    Vector3 direction;

    [Header("---Conditions---")]
    public bool close;
    public bool interactable;
    bool loop;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        angle = transform.eulerAngles.y;
    }

    private void Update()
    {
        if ( Mathf.Round (transform.eulerAngles.y) != angle) // Calculates if the door is open
        {
            transform.Rotate(direction * speed); //Turn the door to close
            close = true;
        }

        if (interactable)
        {
            closeText.SetActive(true); //Activates the door text            

            if (Input.GetKeyDown(KeyCode.E)) //By interacting with the door we give it the parameters of the turn.
            {                
                angle = 0;
                direction = Vector3.up;
            }
        }  
        
        else closeText.SetActive(false);

        if(close) move = clown.GetComponent<Movement>();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Controls the clown's dialogue if he finds the door locked
    /// </summary>
    /// <returns></returns>
    IEnumerator DoorText()
    {
        warningText.SetActive(true);
        yield return new WaitForSeconds(2);
        warningText.SetActive(false);
        loop = true;
    }

    #endregion

    #region Detection Methods
    private void OnTriggerStay(Collider other)
    {   
        if(close && other.tag == "Enemy") // When the clown encounters the closed door, he stops.
        {            
            gm.introduce = false;
            move.activate = false;
            move.walk = false;
            if (!loop) StartCoroutine(DoorText()); //Prevents constant display
        }
    }

    private void OnTriggerExit(Collider other) //Deactivate the "Button" and it is no longer interactive.
    {
        if (other.tag == "Player") closeText.SetActive(false);        
    }

    #endregion
}
