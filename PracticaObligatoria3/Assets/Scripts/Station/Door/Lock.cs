using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    #region Variables

    Door door;

    #endregion

    #region Main Methods

    private void Start()
    {
        door = GetComponentInParent<Door>();
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter(Collider other)
    {
        if (!door.close && other.gameObject.tag == "Player") door.interactable = true; //Detecta al jugador hace que pueda interactuar con la puerta
    }

    private void OnTriggerExit(Collider other)
    {
        if (door.close && other.gameObject.tag == "Player") door.interactable = false;
    }
    #endregion
}
