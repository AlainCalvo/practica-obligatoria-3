using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    #region Variables.

    PlayerController pc;

    [Header("---INTRO---")]
    public GameObject music;
    public GameObject newsOff;

    bool chillOn;
    bool interact;

    [Header("---BATHROOM---")]
    public GameObject toiletDoor;
    public GameObject piss;
    public GameObject empty;

    bool evacuated;

    [Header("---SOFA---")]
    public GameObject sit;
    public GameObject standing;

    bool raised;

    [Header("---EXIT---")]
    public GameObject open;
    public GameObject closed;

    bool drinked;

    [Header("---COFFE---")]
    public GameObject pickable;
    public GameObject mug;
    public GameObject invisible;

    bool caffeinated;

    [Header("---PAUSE---")]
    public GameObject pauseMenu;

    bool gameIsPaused;

    #endregion

    #region Main Methods

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        pc = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        interact = Input.GetKeyDown(KeyCode.E);
        if (music.activeSelf == true && !chillOn) StartCoroutine(TurnOnTheMusic()); //The dialogue that closes the intro and starts the background music begins.
        WaterClose();
        Couch();
        Exit();
        Breakfast();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf); //Open pause screen.
            gameIsPaused = !gameIsPaused;

            if (gameIsPaused)
            {
                Time.timeScale = 0;
            }
            else Resume();
        }

        if (pauseMenu.activeSelf == true) Cursor.lockState = CursorLockMode.None;
        else Cursor.lockState = CursorLockMode.Locked;
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Controls the text after the intro
    /// </summary>
    /// <returns></returns>
    IEnumerator TurnOnTheMusic()
    {
        newsOff.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        newsOff.SetActive(false);
        chillOn = true;
    }

    /// <summary>
    /// Controls the use of the bathroom.
    /// </summary>
    private void WaterClose()
    {        
        if (pc.bathroom) //If we are near the door
        {
            toiletDoor.SetActive(true); //Displays the text to interact with.
            if (interact) //When interacting
            {
                if (!evacuated) //If we have not used it before
                {
                    piss.SetActive(true); //Fade to black for privacy
                    evacuated = true; //We mark it as used
                }
                else empty.SetActive(true); //If it has been used, it indicates that it is no longer needed.
            }
        }
        else //As we move away from the door
        {
            piss.SetActive(false); //The fade-out screen is deactivated
            empty.SetActive(false); //The message that you don't need to use
            toiletDoor.SetActive(false); //Text to interact is disabled
        }

        if (piss.GetComponent<Image>().color.a > 0) pc.canWalk = false; //If it is in the middle of the fade to black, we block the player's movement.
        else pc.canWalk = true; //At the end of the fade we regain control of the player.
    }

    /// <summary>
    /// Controls the use of the sofa.
    /// </summary>
    private void Couch() 
    {
        if (pc.sofa) //If we are near the sofa
        {
            if (!raised) // If we have not interacted with the sofa.
            {
                sit.SetActive(true); //Displays the text to interact with.
                if (interact) //When interacting
                {
                    standing.SetActive(true); //Show the text of the sofa
                    raised = true; //We mark it as interactive
                }
            }
        }
        else
        {
            standing.SetActive(false); //Deactivate the sofa text.
            sit.SetActive(false); //Disable the interact text.
        }
    }

    /// <summary>
    /// Controls the use of the output
    /// </summary>
    private void Exit()
    {
        if (pc.exit) //If we approach the exit door
        {
            open.SetActive(true); //Display text for interaction
            if (interact) //When interacting.
            {
                if (!drinked) //If we haven't had coffee yet.
                {
                    closed.SetActive(true);//Displays the door text.                                                               
                }
                else SceneManager.LoadScene("Station"); //We leave home
            }
        }
        else
        {
            closed.SetActive(false); //Disables the door text
            open.SetActive(false); //Disable text to interact
        }
    }

    /// <summary>
    /// Control the use of coffee
    /// </summary>
    private void Breakfast()
    {
        if (pc.coffe) //If we are close to the cup
        {
            pickable.SetActive(true); //Display text for interaction
            if (interact) //When interacting
            {
                if (!caffeinated) //If we have not had coffee.
                {
                    pc.drink = true; //Activates the animation
                    drinked = true; //Allows to leave the flat
                    caffeinated = true; //Records that the cup has been drunk                    
                }
                else mug.SetActive(true); //If we have already drunk it before, it displays the message that it has already been drunk. 
            }
            if (pc.disappear) invisible.SetActive(false); //While the drinking animation is playing, the cup disappears from the countertop.
            else invisible.SetActive(true); //Re-display the mug at the end of the animation.
        }
        else
        {
            mug.SetActive(false);
            pickable.SetActive(false);
        }
    }

    /// <summary>
    /// Reset the time and close the pause screen.
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        gameIsPaused = false;
    }

    /// <summary>
    /// Back to the main menu from the pause menu
    /// </summary>
    public void LeaveGame()
    {
        gameIsPaused = false;
        SceneManager.LoadScene(0);
    }

    #endregion
}
